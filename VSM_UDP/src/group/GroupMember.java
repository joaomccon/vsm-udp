package group;

import networkSimulation.VirtualMulticastSocket;
import view.ViewMember;


import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.LinkedList;

import messages.AckMsg;
import messages.FlushMsg;
import messages.Msg;

// Falta implementar a thread para tratar de receber os dados do manager
// Ele tem de manter um record das mensagens que enviou e adicionar sempre que recebe um ack
//              Para tal talvez cria-se logo a flushMsg e sempre que se recebe um ack ele adiciona as mensagens estaveis do flush


/*
 * O membro do grupo so fica responsavel por comunicar com o manager
 * A current view e que fica responsavel de comunicar com o multicast
 */
public class GroupMember extends Group{
   
    private int id;
    private VirtualMulticastSocket group_socket;
    private Socket group_manager_socket;
    private ViewMember view;
    private HashMap<Integer, ViewMember> views;
    protected HashMap<Integer, Integer> vector_clock;
    private boolean imediate;
    
    private ByteArrayOutputStream packet_byte_stream;
    private ObjectOutputStream object_stream;
    
    public GroupMember(InetAddress manager, int manager_port, InetAddress group, int group_port) throws UnknownHostException, IOException 
    {
        
        super();
        this.group_manager_socket = new Socket(manager, manager_port);
        this.vector_clock = new HashMap<Integer, Integer>();
        
System.out.println("NOTICE: requesting to joing group managed by " + this.group_manager_socket.getRemoteSocketAddress());
        
        this.group_socket = new VirtualMulticastSocket(group, group_port);
        this.setGroupId(this.hashCode());
        
System.out.println("NOTICE: joined group w/ id " + this.getGroupId() + " @" + this.group_socket.getSocketInetAddress().toString());
        
        this.id = this.getNodeId();
        this.imediate = true;
        
        packet_byte_stream = new ByteArrayOutputStream(4096);
        object_stream = new ObjectOutputStream(new BufferedOutputStream(packet_byte_stream));
    }  
    
    public void makeNonImediate() {
        this.imediate = false;
    }
    
    public Socket getManagerSocket()
    {
        return this.group_manager_socket;
    }
    
    public VirtualMulticastSocket getGroupSocket()
    {
        return this.group_socket;
    }
    
    public HashMap<Integer, ViewMember> getAllViews()
    {
        return this.views;
    }
    
    public void incrementVectorclock()
    {            
        this.vector_clock.put(this.id, this.vector_clock.get(this.id).intValue() + 1);  
    }
    
    public void updateVectorClock(HashMap<Integer, Integer> clock)  // Falta faze a cena de caso incremente mais do que 1
    {
        this.vector_clock = clock;
    }
   
    public HashMap<Integer, Integer> getVectorClock()
    {
        return this.vector_clock;
    }
    
    public void leave() throws InterruptedException, IOException 
    {
        this.join();                        // Termina a thread do Group
        this.group_manager_socket.close();  // Fecha o socket com o manager (nota: nao sei se o manager tem nocao que isso acontece)
        this.group_socket.close();          // Fecha o socket multicast com o grupo
    }
   
   public void cast(byte[] data) throws IOException     // Envia a mensagem
   {
       
       object_stream.writeObject(new Msg(this.id, this.view.getView(), data, this.getVectorClock()));
       this.finalCast();
      
   }
   
   public void castAck(Msg received_msg) throws IOException
   {    
       
       object_stream.writeObject(new AckMsg(this.id, this.view.getView(), received_msg.getSenderId(), received_msg.hashCode()));
       this.finalCast();
   }
   
   public void castFlush() throws IOException
   {
       object_stream.writeObject(new FlushMsg(this.id, this.view.getView(), this.view.getUnstableMessages()));
       this.finalCast();
   }
   
   public void recast() throws IOException
   {
       for(Msg message:this.view.getUnstableMessages())
       {
           object_stream.writeObject(message);
           this.finalCast();
       }
   }
   
   private void finalCast() throws IOException
   {
       this.object_stream.flush();
       
       byte[] buffer = packet_byte_stream.toByteArray();
       
       DatagramPacket packet = new DatagramPacket(buffer, buffer.length, this.group_socket.getInetAddress(), this.group_socket.getPort());
  
       this.group_socket.send(packet);   
       this.object_stream.flush();
   }
   
   
   @SuppressWarnings("unchecked")
public void run() {
       
       DataInputStream data;
       
       try 
       {
           
           data = new DataInputStream(this.group_manager_socket.getInputStream());
           
       } catch (IOException e) 
       {
System.out.println("ERROR: couldn't get input stream from group manager");
            return;
       }
       
       while(this.group_manager_socket.isConnected())
       {
     
           
           try 
           {
               
            LinkedList<Integer> view = (LinkedList<Integer>)new ObjectInputStream(data).readObject();
            
        }catch (ClassNotFoundException | IOException e) 
       {
            
System.out.println("ERROR: unable to read from gourp member socket");

        }
          
           if(this.view.hasUnstableMessages()) 
           {
               
               if(this.imediate)
               {
                   
                   if(this.view.hasUnstableMessages())
                   {
                       try {
                        this.recast();
                    } catch (IOException e) {
                        
                    }
                   }
                   
               }else
               {
                   int next = Integer.MAX_VALUE;
                   for(Integer i : this.views.keySet())
                   {
                       if(i.intValue() < next)
                           next = i.intValue();
                   }
                   
                   try {
                    this.view.join();
                } catch (InterruptedException e) {
System.out.println("ERROR: can't terminate previous view's thread");                   
                }
                   this.view = this.views.get(next);
System.out.println("STATUS: changed to view " + String.valueOf(next) + " on group " + String.valueOf(this.getGroupId()));
               }
               
           }
           
       }
       
   }
    
   public int getNodeId() {
       
       int hash = 8191; // First prime ever recorded
       int prime = 2147483647; // Maior numero primo de 32 bits
       
       hash = prime * hash + this.group_manager_socket.getLocalSocketAddress().hashCode();
       
       return hash;
       
   }
   
   @Override 
   public int hashCode() {
       
       int hash = 8191; // First prime ever recorded
       int prime = 2147483647; // Maior numero primo de 32 bits
       
       hash = prime * hash + (this.group_manager_socket.getInetAddress() + ":" + this.group_manager_socket.getPort()).hashCode();
       
       return hash;
   }
   
}
