package group;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;

import view.ViewManager;

public class GroupManager extends Group
{
    
    private ServerSocket socket;
    private ViewManager view;
    private HashMap<Integer,Socket> members;
    
    public GroupManager(int port) throws IOException 
    {
        
        super();
        
        this.socket = new ServerSocket(port);
        this.setGroupId(this.hashCode());
        this.view = new ViewManager();
        this.members = new HashMap<Integer, Socket>();

System.out.println("STATUS: created group w/ id " + this.getGroupId() + " @" + this.socket.getLocalSocketAddress());
        
        this.start();
    }
    
    public ServerSocket getServerSocket()
    {
        return this.socket;
    }
    
    public void sendView() throws IOException 
    {
System.out.println("STATUS: sending view to members of group " + this.getGroupId());  
        view.sendView(this.members);
        
    }
    
    
    public void kickMember(Socket member) throws IOException 
    {
        // Expulsa um membro do grupo
        // Para tal simplesmente manda uma nova view em que o o elemento que foi expulso n�o faz parte e avisa o membro em quest�o
        
        this.members.remove(this.generateMemberId(member));
        
        try
        {
            if(member.isConnected())
                member.close();
        } catch (IOException e) 
        {
System.out.println("ERROR: couldn't close kicked member's socket, it may have already been closed");
        }
        
        view.sendView(this.members);
        
    }
    
    public void closeGroup() throws InterruptedException, IOException 
    {
        this.join(0);
        
        for(Socket member : this.members.values())
        {
            
            member.close();
            
        }
        
    }
    
    public void run()
    {
        
        Socket new_member = null;
        int new_member_uid = 0;
        
        while(true) 
        {
System.out.println("STATUS: listening for new clients");
            try 
            {
               
                new_member =  this.socket.accept();
                new_member_uid = this.generateMemberId(new_member);
                
                if(this.members.containsKey(new_member_uid))
                {
                    if(this.members.get(new_member_uid).isConnected())
                        new_member.close();
                    else
                        this.members.put(new_member_uid, new_member);
                }     
                else
                    this.members.put(new_member_uid, new_member);
            } catch (IOException e) 
            {         
System.out.println("ERROR: can't connect to client" + new_member_uid + "@" + new_member.getRemoteSocketAddress());
           
            }          
            
System.out.println("NOTICE: new member w/ id " + new_member_uid + " @" + new_member.getRemoteSocketAddress());
            
            try {
            
                this.sendView();
                
            } catch (IOException e) 
            {       
            }
            
        }        
    }
    
    private Integer generateMemberId(Socket member_socket) 
    {
        
        int hash = 8191; // First prime ever recorded
        int prime = 2147483647; // Maior numero primo de 32 bits
        
        hash = prime * hash + member_socket.getRemoteSocketAddress().hashCode();
        
        return hash;
    }
    
    @Override
    public int hashCode()
    {
        int hash = 8191; // First prime ever recorded
        int prime = 2147483647; // Maior numero primo de 32 bits
        
        try {
            hash = prime * hash + (InetAddress.getByName("localhost").toString() + ":" +this.socket.getLocalPort()).hashCode();
        } catch (UnknownHostException e) {
            return -1; 
        }
        
        return hash;
    }
    
}
