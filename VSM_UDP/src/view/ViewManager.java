package view;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.LinkedList;

public class ViewManager extends View{
    
    private static final long serialVersionUID = -840365261537200191L;

    public ViewManager() {
        super();
    }
 
    public void newView() {
        this.incrementCurrentView();
    }
    
    public void sendView(HashMap<Integer,Socket> members) 
    {
        
        for(Socket socket: members.values())    // Itera sobre os socmet que fazem parte da view
        {
System.out.println("STATUS: sending new view to member " + socket.getRemoteSocketAddress());          
            if(socket.isClosed())               // Verifica se o socket que se encontra fechado (para impedir excepcoes o maximo de possivel)
            {
                for(Integer id: members.keySet())   // Caso esteja fechado vai a procura no hashmap do socket em questao e remove-o
                {
                    
                    if(members.get(id) == socket)
                    {
                        members.remove(id);
                        break;
                    }
                    
                }
                
            }
            else
            {
            // Vai buscar o outputstream do socket para poder serializar a view(o hashmap que a representa) (assim manda-se logo o objeto e nao e preciso estar a transforma-lo numa string ou algo do genero) 
            //  Transforma o hashmap numa linked list para so enviar os ids dos membros do grupo em vez de enviar os sockets tambem   
            //Manda a linkedlist que representa os membros do grupo aka view
           
            try {
                new DataOutputStream(socket.getOutputStream()).writeInt(this.getCurrentView());
                new ObjectOutputStream(socket.getOutputStream()).writeObject(new LinkedList<Integer>(members.keySet()));  
            } catch (IOException e) {
System.out.println("ERROR: unable to send view to member @" + socket.getRemoteSocketAddress());
            }
            
            
System.out.println("STATUS: finished sending new view to member " + socket.getRemoteSocketAddress());             
            }
        }
        
    }
    
}
