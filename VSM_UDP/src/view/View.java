package view;

import java.io.Serializable;

public class View extends Thread implements Serializable{

    private static final long serialVersionUID = 5318098398689098462L;
    
    private int current_view = 0;
    
    public View() {

    }

    public int getCurrentView()
    {
        return this.current_view;
    }
    
    protected void incrementCurrentView()
    {
        this.current_view++;
    }
    
}
