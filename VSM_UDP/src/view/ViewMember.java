package view;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.util.HashMap;
import java.util.LinkedList;

import group.GroupMember;
import messages.AckMsg;
import messages.FlushMsg;
import messages.Msg;

public class ViewMember extends View{
    
    private static final long serialVersionUID = -4526382578241138259L;
    
    private int view_id;
    protected LinkedList<Integer> view_members;
    public HashMap<Msg, Boolean> received_msg;
    protected HashMap<Msg, Integer> sent_messages;
    private GroupMember group;
    
    public ViewMember(int view_id, GroupMember group) 
    {   
        super();
      
        this.view_id = view_id;
        this.group = group;
    }
    

    public void newAck(Msg sent_message)
    {
        this.sent_messages.put(sent_message, this.sent_messages.get(sent_message).intValue() + 1);
    }
    
    public int getView()
    {
        return this.view_id;
    }
    
    public boolean hasUnstableMessages()
    {
        for(Integer i:this.sent_messages.values())
        {
            if(i.intValue() < (this.view_members.size() - 1))
                return false;
        }
        
        return true;
    }
    
    public LinkedList<Msg> getUnstableMessages(){
        
        LinkedList<Msg> unstable = new LinkedList<Msg>();
        
        for(Msg message: this.sent_messages.keySet())
        {
            
            if(this.sent_messages.get(message).intValue() < (this.view_members.size() - 1))
            {
                unstable.add(message);
            }
            
        }
          
       return unstable; 
        
    }
  
    
    public LinkedList<Integer> getCurrentViewMembers() {
        return this.view_members;
    }
    
    public void run(){
        
        byte[] buffer = new byte[4096];
        

        ObjectInputStream object_stream;
        Object message_obj = null;
        try {
            
            object_stream = new ObjectInputStream(new BufferedInputStream(new ByteArrayInputStream(buffer)));
            
        } catch (IOException e1) {
System.out.println("ERROR: couldn't open strem to multicast socket");
        return;
        }
        
        while(this.group.getGroupSocket().isConnected())       // Verifica se o socket do grupo ainda esta conectado
        {
            
            try {
                
                this.group.getGroupSocket().receive(new DatagramPacket(buffer, buffer.length));
                
                message_obj = object_stream.readObject();
                
                Msg message = (Msg)message_obj;

                if((message.getView() < this.getView() ) || !this.getCurrentViewMembers().contains(message.getView()) )
                {
 System.out.println("ERROR: invalid message received from " + message.getSenderId());
                }
                else if (message.getView() > this.getView())
                {
                   if(!this.group.getAllViews().containsKey(message.getView()))
                       this.group.getAllViews().put(message.getView(), new ViewMember(message.getView(), this.group));
                   
                   this.group.getAllViews().get(message.getView()).received_msg.put(message, false);
              
                }else
                {
                    /*
                     * tem de discriminar o tipo das mensagens
                     * verificar se ainda ha mensagens nao estaveis na view e se nao manda um flush e tem de esperar pelo acks e muda de view quando os receber
                     */
                    
                    if(message_obj.getClass() == AckMsg.class)
                    {
                        
                        AckMsg ack = (AckMsg) message_obj;
                        
                        for(Msg sent: this.sent_messages.keySet())
                        {
                            if(sent.hashCode() == ack.getMessageId())   // nao se garante que ele nao esta a receber dois acks !!!!!
                            {
                                this.sent_messages.put(sent , this.sent_messages.get(sent).intValue() + 1);
                            }
                        }
                        
                        
                    }else if(message_obj.getClass() == FlushMsg.class)
                    {
                        if(!this.hasUnstableMessages())
                            this.group.castAck((Msg)message_obj);
                        
                    }else{
                        
                        boolean discard = false;
                        
                        for(Msg received: this.received_msg.keySet())
                        {
                            if(received.hashCode() == message.hashCode())
                            {
                                discard = true;
                            }
                        }
                        
                        if(!discard)
                        {
                            this.received_msg.put(message, true);
                            this.group.updateVectorClock(message.getVectorClock());   // Falta ver se caso o vector clock seja mais do que um passo de 1 se se mete num fifo ou numa merda assim parecida
                            this.group.castAck(message);
                        }
                        
                    }
                      
                }

             
             // Precisa de fazer update ao seu vector clock e descartar pacotes que sejam de views anteriores, e guardar as mensagens que sao de uma view futura
         } catch (IOException | ClassNotFoundException e) {
             break;
         }
            
        }
        try {
            object_stream.close();
        } catch (IOException e) {
            return;
        }
        
    }
}
