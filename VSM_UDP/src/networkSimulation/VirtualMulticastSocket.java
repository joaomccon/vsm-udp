package networkSimulation;

import java.io.*;
import java.net.*;

public class VirtualMulticastSocket extends MulticastSocket{
    
    private double drop_rate;
    private double average_delay;
    private double standard_deviation_delay;
    
    private InetSocketAddress group;

    
    @SuppressWarnings("deprecation")
    public VirtualMulticastSocket(InetAddress socket_address, int port) throws IOException{  
        
        super(port);
        
        this.group = new InetSocketAddress(socket_address, port);
              
        this.joinGroup(socket_address);
        
        this.drop_rate = 0;    
        
    }
    
    public InetSocketAddress getSocketInetAddress()
    {
        return this.group;
    }
    
    public void setChannelAttributes(double drop_rate, double average_delay, double standard_deviation_delay) {
        
        this.drop_rate = drop_rate;
        this.average_delay = average_delay;
        this.standard_deviation_delay = standard_deviation_delay;
        
    }
    

    public void send(byte[] data) throws IOException {

        DatagramPacket packet = new DatagramPacket(data,  data.length, this.group);
       
     
        if(Math.random() < drop_rate)
        {
            double delay = Math.random() * this.average_delay + Math.random() * this.standard_deviation_delay;
            
            try {
                this.wait((long)(delay * 1000));
            } catch (InterruptedException e) {
                
            }
            
            this.send(packet);
        }else
        {
            System.out.println("DEBUG: message lost");
        }


    }
    

    public DatagramPacket receive() throws IOException {
        
        byte[] data = new byte[2048];
        
    	DatagramPacket packet = new DatagramPacket(data, data.length, this.group);
    	
		this.receive(packet);
	
		return packet;
			     
    }
    
}
