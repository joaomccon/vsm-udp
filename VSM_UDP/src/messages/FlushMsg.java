package messages;

import java.util.LinkedList;

public class FlushMsg extends Msg{

    private static final long serialVersionUID = 1L;
    
    private LinkedList<Msg> stable_messages;
    
    public FlushMsg(int senderId, int viewId, LinkedList<Msg> stable_messages) {
        super(senderId, viewId);
        this.stable_messages = stable_messages;
    }
    
    public LinkedList<Msg> getStableMessages(){
        return this.stable_messages;
    }
    
    public void newStableMessage(Msg new_stable_message)
    {
        this.stable_messages.add(new_stable_message);
    }
    
    @Override 
    public int hashCode() {
        
        int hash = 8191; // First prime ever recorded
        int prime = 2147483647; // Maior numero primo de 32 bits
        
        for(Msg message: this.stable_messages)
            hash = prime * hash + message.hashCode();
       
        return hash;
    }
    
    
    @Override
    public boolean equals(Object obj)
    {
        
        if(!super.equals(obj))
            return false;
        
        FlushMsg msg = (FlushMsg)obj;
        
        if(this.stable_messages.equals(msg.getStableMessages()))
                return true;
        
        return false;
        
    }
    
}
