package messages;

import java.io.Serializable;
import java.util.HashMap;

// Mandar um ack 
// Mensagens inst�veis s�o mensagens que nem todos os processos ainda n�o receberam (n�o recebereu ack de todos os membros do grupo) mudar isto aqui


public class Msg implements Serializable{
    
    private static final long serialVersionUID = 3145714404088547545L;
    
    private int senderId;
    private int viewId;
    
    private HashMap<Integer, Integer> vector_clock;     // <Value, SenderId/GroupMemberId>
    private byte[] payload;
    
    private void setMessageProperties(int senderId, int viewId) {
        
        this.senderId = senderId;
        this.viewId = viewId;
        
    }
    
    public Msg(int senderId, int viewId) {          // Constructor para mensagem sem payload
        
        super();
        
        this.setMessageProperties(senderId, viewId);
        this.payload = null;
    }
    
    public Msg(int senderId, int viewId, byte[] payload, HashMap<Integer, Integer> vector_clock) {  // Constructor para caso a mensagem tenha payload
        
        super();
        this.setMessageProperties(senderId, viewId);
        this.payload = payload;
        this.vector_clock = vector_clock;
        
    }
    
    public int getView() {            // Retorna a view a que a mensagem pertence
        return this.viewId;
    }
    
    public int getSenderId() {          // Retorna o id do elemento que enviou a mensagem 
        return this.senderId;
    }
    
    public byte[] getPayload() {        // Retorna o payload da mensagem
        return this.payload;
    }
    
    public HashMap <Integer, Integer> getVectorClock() {    // Retorna o sequence number da mensagem caso ela tenha paylaod
        return this.vector_clock;
    }
    
    @Override
    public boolean equals(Object object) {
        
        if(this == object)     //Caso object seja um pointer para a pr�pria classe da mensagem
            return true;
        else if ((object == null) || (this.getClass() != object.getClass()))    // Caso object seja um null pointer ou seja de uma classe diferente
            return false;
        
        Msg msg = (Msg) object; // Cast do Object para classe do tipo Msg (para se poder fazer os compares necessarios)
        
        if(this.hashCode() != msg.hashCode())
            return false;
                    
         return true;
    }
    
     @Override
     public int hashCode() {
         
         int hash = 8191; // First prime number ever recorded
         int prime = 2147483647; // Largest 32 bit prime number
         
         hash = prime * hash + this.getSenderId();
         hash = prime * hash + this.getView();
         
         if(this.vector_clock != null)
             hash = prime * hash + this.vector_clock.hashCode();
         
         if(this.getPayload() != null)
             hash = prime * hash + this.getPayload().hashCode();
         
         return hash;
     }
    
}
