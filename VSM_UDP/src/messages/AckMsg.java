package messages;

public class AckMsg extends Msg{

  
    private static final long serialVersionUID = -2618630169920557527L;
    
    private int msg_sender_id;
    private int msg_id;

    public AckMsg(int senderId, int viewId, int msg_sender_id, int msg_id) 
    {
        
        super(senderId, viewId);
        this.msg_sender_id = msg_sender_id;
        this.msg_id = msg_id;
    }
    
    public int getMessageSenderId() 
    {
        return this.msg_sender_id;
    }
    
    public int getMessageId()
    {
        return this.msg_id;
    }
    
}
