package actors;


import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;


import group.GroupMember;

public class Process {

    private HashMap<Integer, GroupMember> joined_groups;
    //EXEMPLO
    //java Process ip_manager port_manager ip_group port_group (d) --> d(deferida) ou i(imediata)
    public static void main(String[] args) throws UnknownHostException, IOException, ClassNotFoundException {
        InetAddress mAddr = InetAddress.getByName(args[0]);
        int mPort = Integer.valueOf(args[1]);
        
        InetAddress gAddr = InetAddress.getByName(args[2]);
        int gPort = Integer.valueOf(args[3]);
        
        Process self = new Process();
                
        self.joinGroup(mAddr, mPort, gAddr, gPort);
        self.castToGroup(1, args[2].getBytes()); 
        
        if(args[4]=="d") {
        	//deferida
        	
        }
        else if(args[4]=="i") {
        	//imediata
        }
      
    }
    
    public Process() {
        
        this.joined_groups = new HashMap<Integer, GroupMember>();
        
    }
    
    public void joinGroup(InetAddress manager, int manager_port, InetAddress group, int group_port) throws UnknownHostException, IOException{
        
        GroupMember new_group = new GroupMember(manager, manager_port, group, group_port);
        
        this.joined_groups.put(Integer.valueOf(new_group.getGroupId()), new_group);
        
    }
    
    public void castToGroup(int group_id, byte[]data) throws IOException {

        if(this.joined_groups.containsKey(Integer.valueOf(group_id))) {
            this.joined_groups.get(Integer.valueOf(group_id)).cast(data);
        }
    }
    
    
}
