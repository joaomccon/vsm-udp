package actors;

import java.io.IOException;
import java.util.HashMap;

import group.GroupManager;


public class Manager {

    private HashMap<Integer, GroupManager> groups;
    private int baseSocket = 5000;
    

    public static void main(String[] args) throws IOException {
        
        Manager self = new Manager();
        
        self.newGroup();
   
    }
    
    public Manager() {
        
        this.groups = new HashMap<Integer, GroupManager>();
        
    }
    
    public void newGroup() throws IOException {
        
        GroupManager new_group = new GroupManager(this.baseSocket++);
      
        this.groups.put(new_group.hashCode(), new_group);

    }
    
    public GroupManager getGroup(int id) {
        
        if(!this.groups.containsKey(id))
            return null;
        
        return this.groups.get(id);
    }
    
}

